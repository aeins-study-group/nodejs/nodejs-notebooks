const PENDING = 'pending'
const FULFILLED = 'fulfilled'
const REJECTED = 'rejected'
class APromise {
  constructor (executor) {
    this._status = PENDING
    this.result = undefined
    this.callbacks = []
    this.errbacks = []
    try {
      executor(this._resolve, this._reject)
    } catch (e) {
      this.reject(e)
    }
  }

  get status () {
    return this._status
  }

  set status (newStatus) {
    this._status = newStatus
    switch (newStatus) {
      case FULFILLED:
        this.callbacks.forEach(callback => {
          callback(this.result)
        })
        break
      case REJECTED:
        this.errbacks.forEach(errback => {
          errback(this.result)
        })
        break
    }
  }

  _resolve = value => {
    if (this.status === PENDING) {
      this.result = value
      this.status = FULFILLED
    }
  }

  _reject = reason => {
    if (this.status === PENDING) {
      this.result = reason
      this.status = REJECTED
    }
  }

  isFunction (func) {
    return typeof func === 'function'
  }

  then (onFulfilled, onRejected) {
    const realOnFulfilled = this.isFunction(onFulfilled) ? onFulfilled : value => value
    const realOnRejected = this.isFunction(onRejected) ? onRejected : reason => { throw reason }
    const promise2 = new APromise((resolve, reject) => {
      const fulfilledMicroTask = () => {
        queueMicrotask(() => {
          try {
            const x = realOnFulfilled(this.result)
            this.resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        })
      }

      const rejectedMicroTask = () => {
        queueMicrotask(() => {
          try {
            const x = realOnRejected(this.result)
            this.resolvePromise(promise2, x, resolve, reject)
          } catch (e) {
            reject(e)
          }
        })
      }
      switch (this.status) {
        case FULFILLED:
          fulfilledMicroTask()
          break
        case REJECTED:
          rejectedMicroTask()
          break
        case PENDING:
          this.callbacks.push(fulfilledMicroTask)
          this.errbacks.push(rejectedMicroTask)
      }
    })
    return promise2
  }

  resolvePromise (promise, x, resolve, reject) {
    if (promise === x) {
      return reject(new TypeError())
    }
    if (x instanceof APromise) {
      queueMicrotask(() => {
        x.then(y => {
          this.resolvePromise(promise, y, resolve, reject)
        }, reject)
      })
    } else if (typeof x === 'object' || this.isFunction(x)) {
      if (x === null) {
        return resolve(x)
      }
      let then = null
      try {
        then = x.then
      } catch (e) {
        return reject(e)
      }
      if (this.isFunction(then)) {
        let called = false
        try {
          then.call(x,
            value => {
              if (called) return
              called = true
              this.resolvePromise(promise, value, resolve, reject)
            },
            reason => {
              if (called) return
              called = true
              reject(reason)
            })
        } catch (e) {
          if (called) return
          reject(e)
        }
      } else {
        resolve(x)
      }
    } else {
      resolve(x)
    }
  }

  static resolve (value) {
    if (value instanceof APromise) {
      return value
    }
    return new APromise(resolve => {
      resolve(value)
    })
  }

  static reject (reason) {
    return new APromise((resolve, reject) => {
      reject(reason)
    })
  }

  finally (callback) {
    return this.then(
      value => {
        return APromise.resolve(callback()).then(() => value)
      },
      reason => {
        return APromise.resolve(callback()).then(() => { throw reason })
      }
    )
  }

  catch (onRejected) {
    return this.then(null, onRejected)
  }

  static deferred () {
    const tester = Object.create(null)
    tester.promise = new APromise((resolve, reject) => {
      tester.resolve = resolve
      tester.reject = reject
    })
    return tester
  }
}

module.exports = APromise
